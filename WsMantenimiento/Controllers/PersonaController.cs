﻿using Capa_Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WsMantenimiento.Controllers
{
    [RoutePrefix("api/Persona")]
    public class PersonaController : ApiController
    {
        [Route("ObtenerPersonas")]
        [HttpPost]
        //[EnableCors]
        public Response Get()
        {
            try
            {
                var resp = Capa_Negocio.N_Persona.ObtenerPersonas();
                return new Response
                {
                    status = "OK",
                    message = "Respuesta Satisfactoria",
                    content = resp
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    status = "Error",
                    message = ex.Message,
                    content = null
                };
            }
        }
    }
}
