﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Entidad
{
    public class Response
    {
        public string status { get; set; }
        public object content { get; set; }
        public string message { get; set; }
    }
}
