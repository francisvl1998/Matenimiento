﻿using Capa_Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Negocio
{
    public class N_Persona
    {
        public static List<E_Persona> ObtenerPersonas()
        {
            return Capa_Datos.D_Persona.ObtenerPersonas();
        }
    }
}
