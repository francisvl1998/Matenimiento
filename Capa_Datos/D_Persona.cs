﻿using Capa_Entidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa_Datos
{
    public class D_Persona
    {
        public static List<E_Persona> ObtenerPersonas()
        {
            List<E_Persona> lstPersona = new List<E_Persona>();
            SqlCommand cmd = new SqlCommand();
            Conexion con = new Conexion();

            try
            {
                cmd.Connection = con.cadena;
                cmd.Connection.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "sp_obt_personas";

                using (SqlDataReader lector = cmd.ExecuteReader())
                {
                    while (lector.Read())
                    {
                        E_Persona persona = new E_Persona();
                        persona.Id = Convert.ToInt32(lector["Id"]);
                        persona.PrimerNombre = lector["PrimerNombre"] as string ?? "";
                        persona.SegundoNombre = lector["SegundoNombre"] as string ?? "";
                        persona.PrimerApellido = lector["PrimerApellido"] as string ?? "";
                        persona.SegundoApellido = lector["SegundoApellido"] as string ?? "";
                        persona.Edad = Convert.ToInt32(lector["Edad"]);
                        persona.TipoDocumento = lector["TipoDocumento"] as string ?? "";
                        persona.NumeroDocumento = lector["NumeroDocumento"] as string ?? "";
                        lstPersona.Add(persona);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            finally
            {
                con.cadena.Close();
                cmd.Dispose();
            }
            return lstPersona;
        }
    }
}
